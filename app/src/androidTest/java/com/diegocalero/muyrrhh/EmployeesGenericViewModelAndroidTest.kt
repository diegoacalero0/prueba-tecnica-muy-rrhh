package com.diegocalero.muyrrhh


import android.app.Application
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.diegocalero.muyrrhh.models.Employee
import com.diegocalero.muyrrhh.viewmodels.EmployeesGenericViewModel
import io.mockk.mockk
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.internal.util.reflection.FieldSetter


@RunWith(AndroidJUnit4::class)
class EmployeesGenericViewModelAndroidTest {

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    private lateinit var viewModel: EmployeesGenericViewModel

    lateinit var application: Application

    @Before
    fun setUp() {
        application = mockk(relaxUnitFun = true)
        viewModel = EmployeesGenericViewModel(application)
    }

    @Test
    fun toggleShowAllTest() {
        FieldSetter.setField(
            viewModel,
            viewModel::class.java.getDeclaredField("_showAll"),
            MutableLiveData(true)
        )

        viewModel.toggleShowAll()

        Assert.assertFalse(viewModel.showAll.value?:true)
        Assert.assertEquals("", viewModel.searchText.value)
    }

    @Test
    fun getAmountEmployeesTest() {
        Assert.assertEquals(0, viewModel.amountEmployees.value)

        FieldSetter.setField(
            viewModel,
            viewModel::class.java.getDeclaredField("_amountEmployees"),
            MutableLiveData(6)
        )

        Assert.assertEquals(6, viewModel.amountEmployees.value)
    }

    @Test
    fun getAmountNewEmployeesTest() {
        Assert.assertEquals(0, viewModel.amountNewEmployees.value)

        FieldSetter.setField(
            viewModel,
            viewModel::class.java.getDeclaredField("_amountNewEmployees"),
            MutableLiveData(11)
        )

        Assert.assertEquals(11, viewModel.amountNewEmployees.value)
    }

    @Test
    fun searchTest() {

        val employees = ArrayList<Employee>()
        employees.add(Employee(1, "Argemiro", "CEO", 5000, null, true))
        employees.add(Employee(1, "Belal", "CEO", 5000, null, false))
        employees.add(Employee(1, "Carcanues", "CEO", 5000, null, false))

        Assert.assertFalse(viewModel.showEmpty.value?:true)

        FieldSetter.setField(viewModel, viewModel::class.java.getDeclaredField("allEmployees"), employees)
        viewModel.search("ar")

        Assert.assertEquals(2, viewModel.employeesList.value?.size)

        FieldSetter.setField(viewModel, viewModel::class.java.getDeclaredField("_showAll"), MutableLiveData(false))
        viewModel.search("ar")

        Assert.assertEquals(1, viewModel.employeesList.value?.size)

        viewModel.search("hola")
        Assert.assertTrue(viewModel.showEmpty.value?:false)

    }

    @Test
    fun sortEmployeesTest() {
        val employees = ArrayList<Employee>()
        employees.add(Employee(1, "Argemiro", "CEO", 1000, null, true))
        employees.add(Employee(1, "Belal", "CEO", 2000, null, false))
        employees.add(Employee(1, "Carcanues", "CEO", 500, null, false))

        val mutableEmployees = MutableLiveData<ArrayList<Employee>>(employees)

        FieldSetter.setField(viewModel, viewModel::class.java.getDeclaredField("employeesList"), mutableEmployees)

        viewModel.sortEmployees(true)
        Assert.assertEquals(500L, viewModel.employeesList.value?.get(0)?.wage)
        Assert.assertEquals(1000L, viewModel.employeesList.value?.get(1)?.wage)
        Assert.assertEquals(2000L, viewModel.employeesList.value?.get(2)?.wage)

        viewModel.sortEmployees(false)
        Assert.assertEquals(2000L, viewModel.employeesList.value?.get(0)?.wage)
        Assert.assertEquals(1000L, viewModel.employeesList.value?.get(1)?.wage)
        Assert.assertEquals(500L, viewModel.employeesList.value?.get(2)?.wage)

    }

}