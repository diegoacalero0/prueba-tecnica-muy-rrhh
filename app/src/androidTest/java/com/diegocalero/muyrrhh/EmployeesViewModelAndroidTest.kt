package com.diegocalero.muyrrhh


import android.app.Application
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.diegocalero.muyrrhh.models.Employee
import com.diegocalero.muyrrhh.viewmodels.EmployeesGenericViewModel
import com.diegocalero.muyrrhh.viewmodels.EmployeesViewModel
import io.mockk.mockk
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.internal.util.reflection.FieldSetter


@RunWith(AndroidJUnit4::class)
class EmployeesViewModelAndroidTest {

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    private lateinit var viewModel: EmployeesViewModel
    private lateinit var genericViewModel: EmployeesGenericViewModel

    lateinit var application: Application

    @Before
    fun setUp() {
        application = mockk(relaxUnitFun = true)
        viewModel = EmployeesViewModel(application)
        genericViewModel = EmployeesGenericViewModel(application)
    }

    @Test
    fun collectTest() {

        val employees = ArrayList<Employee>()
        employees.add(Employee(1, "Argemiro", "CEO", 5000, null, false))
        employees.add(Employee(2, "Belal", "CEO", 5000, null, false))
        employees.add(Employee(3, "Carcanues", "CEO", 5000, null, false))


        val set = HashSet<Int>()
        set.add(3)

        FieldSetter.setField(viewModel, genericViewModel::class.java.getDeclaredField("allEmployees"), employees)
        viewModel.collect(set)

        Assert.assertTrue(viewModel.employeesList.value?.get(2)?.isNew?:false)
    }

}