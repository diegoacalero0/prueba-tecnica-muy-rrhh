package com.diegocalero.muyrrhh.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class Employee (
	@SerializedName("id") val id : Int,
	@SerializedName("name") val name : String,
	@SerializedName("position") val position : String? = "Sin posición",
	@SerializedName("wage") val wage : Long? = 0,
	@SerializedName("employees") val employees : List<Employee>? = ArrayList(),
	@SerializedName("isNew") var isNew: Boolean = false
): Serializable