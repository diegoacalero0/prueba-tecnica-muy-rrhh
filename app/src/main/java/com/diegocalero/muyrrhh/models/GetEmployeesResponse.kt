package com.diegocalero.muyrrhh.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class GetEmployeesResponse (

	@SerializedName("company_name") val company_name : String,
	@SerializedName("address") val address : String,
	@SerializedName("employees") val employees : List<Employee>
): Serializable