package com.diegocalero.muyrrhh.viewmodels

import android.app.Application
import androidx.lifecycle.*
import com.diegocalero.muyrrhh.R
import com.diegocalero.muyrrhh.models.Employee
import com.diegocalero.muyrrhh.models.GetEmployeesResponse
import com.diegocalero.muyrrhh.views.utilities.FilterUtilities
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class DetailViewModel(app: Application, employeeId: Int, employeesResponse: GetEmployeesResponse): EmployeesGenericViewModel(app) {

    private val employeeId = employeeId
    private val detailEmployeesResponse = employeesResponse

    private val _employee = MutableLiveData<Employee>()
    val employee: LiveData<Employee>
        get() = _employee

    val getEmployeeId: LiveData<String> = Transformations.map(employee, ::getEmployeeIdFunction)
    fun getEmployeeIdFunction(employee: Employee): String {
        return context.resources.getString(R.string.employee_id, employee.id)
    }

    val getEmployeePosition: LiveData<String> = Transformations.map(employee, ::getEmployeePositionFunction)
    fun getEmployeePositionFunction(employee: Employee): String {
        return context.resources.getString(R.string.employee_position, employee.position)
    }

    val getEmployeeWage: LiveData<String> = Transformations.map(employee, ::getEmployeeWageFunction)
    fun getEmployeeWageFunction(employee: Employee): String {
        return context.resources.getString(R.string.employee_wage, employee.wage)
    }

    lateinit var detailNewEmployeeObserver: Observer<HashSet<Int>?>

    init {

        detailNewEmployeeObserver = Observer {collect(it)}
        dataStoreUtilities.newEmployeesFlow.asLiveData().observeForever(detailNewEmployeeObserver)

        initEmployee()
        getEmployees()
    }

    private fun initEmployee() {
        detailEmployeesResponse.employees.map { employee ->
            if(employee.id == employeeId) {
                _employee.value = employee
                return
            }
        }
    }

    private fun getEmployees() {
        detailEmployeesResponse.let {
            _amountEmployees.postValue(it.employees.size)
            _employeesResponse.postValue(it)
            _loading.postValue(false)
            _amountEmployees.postValue(it.employees.size)

            val arrayListEmployees = ArrayList<Employee>()
            arrayListEmployees.addAll(it.employees)
            val sortedEmployees = FilterUtilities.sortEmployees(arrayListEmployees, true)

            sortedEmployees.map {sortedEmployee ->
                _employee.value?.employees?.map {
                    if(sortedEmployee.id == it.id) {
                        allEmployees.add(sortedEmployee)
                    }
                }
            }

            if(allEmployees.size == 0) {
                _showEmpty.postValue(true)
            }
        }
    }

    fun collect(it: HashSet<Int>?) {
        it?.let {set ->
            newEmployeesSet = set
            if(set.contains(employeeId)) {
                val employee = _employee.value
                employee?.isNew = true
                _employee.postValue(employee)
            }

            var amountNewEmployees = 0
            allEmployees.map { employee ->
                if(set.contains(employee.id)) {
                    employee.isNew = true
                    amountNewEmployees++
                }
            }

            _amountNewEmployees.postValue(amountNewEmployees)
            this@DetailViewModel.employeesList.postValue(allEmployees)
        }
    }

    fun toggleCheckNewEmployee(isCheked: Boolean) {
        _employee.value?.isNew = isCheked

        if(isCheked) {
            newEmployeesSet.add(employeeId)
        } else {
            newEmployeesSet.remove(employeeId)
        }

        coroutineScop.launch {
            dataStoreUtilities.setNewEmployeesMap(newEmployeesSet)
        }
    }

    override fun onCleared() {
        super.onCleared()
        dataStoreUtilities.newEmployeesFlow.asLiveData().removeObserver(detailNewEmployeeObserver)
    }

}