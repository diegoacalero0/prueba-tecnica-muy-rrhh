package com.diegocalero.muyrrhh.viewmodels

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.diegocalero.muyrrhh.models.GetEmployeesResponse

class ListDetailViewModelFactory(private val application: Application, private val employeeId: Int, private val employeesResponse: GetEmployeesResponse):
    ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if(modelClass.isAssignableFrom(DetailViewModel::class.java)) {
            return DetailViewModel(application, employeeId, employeesResponse) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}