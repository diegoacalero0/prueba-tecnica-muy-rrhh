package com.diegocalero.muyrrhh.viewmodels

import android.app.Application
import androidx.lifecycle.*
import com.diegocalero.muyrrhh.R
import com.diegocalero.muyrrhh.models.Employee
import com.diegocalero.muyrrhh.models.GetEmployeesResponse
import com.diegocalero.muyrrhh.repositories.EmployeesRepository
import com.diegocalero.muyrrhh.utilities.DataStoreUtilities
import com.diegocalero.muyrrhh.views.adapters.EmployeesAdapter
import com.diegocalero.muyrrhh.views.utilities.FilterUtilities
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class EmployeesViewModel(app: Application): EmployeesGenericViewModel(app) {


    private val _companyName = MutableLiveData<String>()
    val companyName: LiveData<String>
        get() = _companyName

    private val _companyAddress = MutableLiveData<String>()
    val companyAddress: LiveData<String>
        get() = _companyAddress

    private var newEmployeesObserver: Observer<HashSet<Int>?>

    init {

        newEmployeesObserver = Observer { collect(it) }
        dataStoreUtilities.newEmployeesFlow.asLiveData().observeForever(newEmployeesObserver)

        getEmployees()
    }

    fun getEmployees() {
        coroutineScop.launch {
            val employees = EmployeesRepository.getEmployees()
            employees?.let {

                _employeesResponse.postValue(it)
                _loading.postValue(false)
                _companyName.postValue(it.company_name)
                _companyAddress.postValue(it.address)
                _amountEmployees.postValue(it.employees.size)

                val arrayListEmployees = ArrayList<Employee>()
                arrayListEmployees.addAll(it.employees)
                val sortedEmployees = FilterUtilities.sortEmployees(arrayListEmployees, true)
                allEmployees.addAll(sortedEmployees)

                dataStoreUtilities.newEmployeesFlow.collect {
                    collect(it)
                }

            }
        }
    }

    fun collect(it: HashSet<Int>?) {
        it?.let {set ->
            newEmployeesSet = it

            allEmployees.map { employee ->
                employee.isNew = set.contains(employee.id)
            }
            this@EmployeesViewModel.employeesList.postValue(allEmployees)
        }
    }

    override fun onCleared() {
        super.onCleared()
        dataStoreUtilities.newEmployeesFlow.asLiveData().removeObserver(newEmployeesObserver)
    }

}