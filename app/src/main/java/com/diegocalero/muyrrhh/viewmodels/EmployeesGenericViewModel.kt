package com.diegocalero.muyrrhh.viewmodels

import android.app.Application
import androidx.lifecycle.*
import com.diegocalero.muyrrhh.R
import com.diegocalero.muyrrhh.models.Employee
import com.diegocalero.muyrrhh.models.GetEmployeesResponse
import com.diegocalero.muyrrhh.repositories.EmployeesRepository
import com.diegocalero.muyrrhh.utilities.DataStoreUtilities
import com.diegocalero.muyrrhh.views.adapters.EmployeesAdapter
import com.diegocalero.muyrrhh.views.utilities.FilterUtilities
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

open class EmployeesGenericViewModel(app: Application): AndroidViewModel(app) {

    val context = app

    private var increasing = true

    protected var newEmployeesSet = HashSet<Int>()

    protected val dataStoreUtilities = DataStoreUtilities(context)

    protected val allEmployees = ArrayList<Employee>()
    val employeesList = MutableLiveData<ArrayList<Employee>>()

    protected var viewModelJob = Job()
    protected val coroutineScop = CoroutineScope(viewModelJob + Dispatchers.Main)

    protected val _adapter = EmployeesAdapter(ArrayList())
    val adapter: EmployeesAdapter
        get() = _adapter

    protected lateinit var employeesListObserver: Observer<ArrayList<Employee>>

    protected val _loading = MutableLiveData<Boolean>(true)
    val loading: LiveData<Boolean>
        get() = _loading

    protected val _showAll = MutableLiveData<Boolean>(true)
    val showAll: LiveData<Boolean>
        get() = _showAll

    protected val _showEmpty = MutableLiveData<Boolean>(false)
    val showEmpty: LiveData<Boolean>
        get() = _showEmpty

    protected val _employeesResponse = MutableLiveData<GetEmployeesResponse>()
    val employeesResponse: LiveData<GetEmployeesResponse>
        get() = _employeesResponse

    protected val _amountEmployees = MutableLiveData<Int>(0)
    val amountEmployees: LiveData<Int>
        get() = _amountEmployees

    protected val _amountNewEmployees = MutableLiveData<Int>(0)
    val amountNewEmployees: LiveData<Int>
        get() = _amountNewEmployees

    val getAmountEmployees: LiveData<String> = Transformations.map(amountEmployees, ::getAmountEmployeesFunction)
    val getAmountNewEmployees: LiveData<String> = Transformations.map(amountNewEmployees, ::getAmountNewEmployeesFunction)

    private val _searchText = MutableLiveData<String>("")
    val searchText: LiveData<String>
        get() = _searchText

    init {
        initObservers()
    }

    fun initObservers() {
        employeesListObserver = Observer {

            var amountNews = 0
            it.map {
                if(it.isNew) {
                    amountNews++
                }
            }

            _amountNewEmployees.value = amountNews
            adapter.employeesList = it
            adapter.notifyDataSetChanged()
        }

        employeesList.observeForever(employeesListObserver)
    }

    fun toggleShowAll() {
        _showAll.value = when(_showAll.value) {
            null, false -> true
            else -> false
        }

        search(_searchText.value?:"")
    }

    private fun getAmountEmployeesFunction(it: Int): String {
        if(it == 0) {
            return context.resources.getString(R.string.employees_amount_empty)
        } else {
            return context.resources.getString(R.string.employees_amount, it)
        }
    }

    private fun getAmountNewEmployeesFunction(it: Int): String {
        if(it == 0) {
            return context.resources.getString(R.string.employees_amount_new_empty)
        } else if(it == 1) {
            return context.resources.getString(R.string.employees_amount_one)
        } else {
            return context.resources.getString(R.string.employees_amount_news, it)
        }
    }

    fun search(text: String) {
        _searchText.value = text

        val resultEmployeesList = ArrayList<Employee>()

        allEmployees.map {
            if(FilterUtilities.isSubstring(text.toLowerCase(), it.name.toLowerCase())) {
                if(_showAll.value == true) {
                    resultEmployeesList.add(it)
                } else {
                    if(it.isNew) {
                        resultEmployeesList.add(it)
                    }
                }
            }
        }

        employeesList.value = resultEmployeesList

        _showEmpty.value = resultEmployeesList.size == 0
        sortEmployees(increasing)
    }

    fun sortEmployees(increasing: Boolean) {
        this.increasing = increasing
        employeesList.value?.let {
            val sortedEmployees = FilterUtilities.sortEmployees(it, increasing)
            employeesList.value = sortedEmployees as ArrayList
        }
    }

    override fun onCleared() {
        super.onCleared()
        employeesList.removeObserver(employeesListObserver)
    }

}