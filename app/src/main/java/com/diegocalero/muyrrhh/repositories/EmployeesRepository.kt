package com.diegocalero.muyrrhh.repositories

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.diegocalero.muyrrhh.models.GetEmployeesResponse
import com.diegocalero.muyrrhh.services.EmployeesApi

object EmployeesRepository {

    suspend fun getEmployees(): GetEmployeesResponse? {
        val deferred = EmployeesApi.retrofitService.getEmployees()

        var response: GetEmployeesResponse? = null

        try {
            response = deferred.await()
        } catch (e: Exception) {
            Log.d("Repository", "${e}")
        }

        return response
    }
}