package com.diegocalero.muyrrhh.views.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import com.diegocalero.muyrrhh.R
import com.diegocalero.muyrrhh.databinding.ItemlistEmployeeBinding
import com.diegocalero.muyrrhh.models.Employee
import com.diegocalero.muyrrhh.views.viewholders.EmployeeViewHolder

class EmployeesAdapter(var employeesList : List<Employee>) : RecyclerView.Adapter<EmployeeViewHolder>() {

    val selectedIndex = MutableLiveData<Int>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EmployeeViewHolder {
        val layoutInflater: LayoutInflater = LayoutInflater.from(parent.context)
        val binding: ItemlistEmployeeBinding = DataBindingUtil.inflate(layoutInflater, R.layout.itemlist_employee, parent, false)


        return EmployeeViewHolder(binding)
    }


    override fun getItemCount(): Int {
        return employeesList.size
    }

    override fun onBindViewHolder(holder: EmployeeViewHolder, position: Int) {
        holder.setEmployee(employee = employeesList[position])

        holder.binding?.root?.setOnClickListener {
            selectedIndex.value = position
        }

    }
}