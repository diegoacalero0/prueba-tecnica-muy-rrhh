package com.diegocalero.muyrrhh.views.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CompoundButton
import android.widget.RadioGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.diegocalero.muyrrhh.R
import com.diegocalero.muyrrhh.databinding.FragmentDetailBinding
import com.diegocalero.muyrrhh.viewmodels.DetailViewModel
import com.diegocalero.muyrrhh.viewmodels.ListDetailViewModelFactory

class DetailFragment : BaseFragment() {

    private lateinit var binding: FragmentDetailBinding
    private lateinit var viewModel: DetailViewModel
    private val args: DetailFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_detail, container, false)
        viewModel = ViewModelProvider(this, ListDetailViewModelFactory(activity?.application!!, args.employeeId, args.employeesResponse)).get(DetailViewModel::class.java)
        binding.viewModel = viewModel


        setOnEditorActionListener(binding.includeEmployeesAndSearch.edittextSearch)
        setOnChangeTextListener(binding.includeEmployeesAndSearch.edittextSearch, viewModel::search)
        initSpinner(binding.includeEmployeesAndSearch.spinnerSort, viewModel::sortEmployees)
        initCheckNewEmployee()


        viewModel.adapter.selectedIndex.observe(viewLifecycleOwner, Observer {
            it?.let {
                val employeeId = viewModel.employeesList?.value?.get(it)?.id
                val employeesResponse = viewModel.employeesResponse.value

                if(employeeId != null && employeesResponse != null) {
                    findNavController().navigate(DetailFragmentDirections.actionDetailFragmentSelf(employeeId, employeesResponse))
                    viewModel.adapter.selectedIndex.value = null
                } else {

                }
            }
        })

        binding.includeEmployeesAndSearch.recyclerviewEmployees.adapter = viewModel.adapter
        binding.includeEmployeesAndSearch.recyclerviewEmployees.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)

        binding.lifecycleOwner = this
        return binding.root
    }

    private fun initCheckNewEmployee() {
        binding.checkboxNewEmployee.setOnClickListener {
            viewModel.toggleCheckNewEmployee(binding.checkboxNewEmployee.isChecked)
        }
    }
}