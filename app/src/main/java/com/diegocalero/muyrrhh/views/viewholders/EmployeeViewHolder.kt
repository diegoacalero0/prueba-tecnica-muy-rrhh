package com.diegocalero.muyrrhh.views.viewholders

import androidx.recyclerview.widget.RecyclerView
import com.diegocalero.muyrrhh.databinding.ItemlistEmployeeBinding
import com.diegocalero.muyrrhh.models.Employee

class EmployeeViewHolder(binding: ItemlistEmployeeBinding) : RecyclerView.ViewHolder(binding.root){
    var binding: ItemlistEmployeeBinding? = null

    init {
        this.binding = binding
    }

    fun setEmployee(employee: Employee){
        binding?.employee = employee
        binding?.executePendingBindings()
    }
}