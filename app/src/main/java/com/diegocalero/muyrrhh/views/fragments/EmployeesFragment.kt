package com.diegocalero.muyrrhh.views.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.diegocalero.muyrrhh.R
import com.diegocalero.muyrrhh.databinding.FragmentEmployeesBinding
import com.diegocalero.muyrrhh.viewmodels.EmployeesViewModel


class EmployeesFragment : BaseFragment() {

    private lateinit var binding: FragmentEmployeesBinding
    private val viewModel: EmployeesViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_employees, container, false)
        binding.viewModel = viewModel


        setOnEditorActionListener(binding.includeEmployeesAndSearch.edittextSearch)
        setOnChangeTextListener(binding.includeEmployeesAndSearch.edittextSearch, viewModel::search)
        initSpinner(binding.includeEmployeesAndSearch.spinnerSort, viewModel::sortEmployees)

        binding.includeEmployeesAndSearch.recyclerviewEmployees.adapter = viewModel.adapter
        binding.includeEmployeesAndSearch.recyclerviewEmployees.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)

        viewModel.adapter.selectedIndex.observe(viewLifecycleOwner, Observer {
            it?.let {
                val employeeId = viewModel.employeesList?.value?.get(it)?.id
                val employeesResponse = viewModel.employeesResponse.value

                if(employeeId != null && employeesResponse != null) {
                    findNavController().navigate(EmployeesFragmentDirections.actionEmployeesFragmentToDetailFragment(employeeId, employeesResponse))
                    viewModel.adapter.selectedIndex.value = null
                } else {

                }
            }
        })

        binding.lifecycleOwner = this
        return binding.root
    }
}