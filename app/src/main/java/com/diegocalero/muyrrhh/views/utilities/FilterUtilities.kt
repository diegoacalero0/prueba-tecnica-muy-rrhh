package com.diegocalero.muyrrhh.views.utilities

import com.diegocalero.muyrrhh.models.Employee

object FilterUtilities {

    /**
     * Function to QuickSort Employees
     */
    fun sortEmployees(employees: ArrayList<Employee>, increasing: Boolean): List<Employee> {
        if (employees.count() < 2){
            return employees
        }
        val pivot = employees[employees.count()/2].wage?:0

        val equal = ArrayList<Employee>()
        employees.map {
            if(it.wage == pivot) {
                equal.add(it)
            }
        }

        val less = ArrayList<Employee>()
        employees.map {
            if(it.wage?:0 < pivot) {
                less.add(it)
            }
        }


        val greater = ArrayList<Employee>()
        employees.map {
            if(it.wage?:0 > pivot) {
                greater.add(it)
            }
        }

        if(increasing) {
            return sortEmployees(less, increasing) + equal + sortEmployees(greater, increasing)
        } else {
            return sortEmployees(greater, increasing) + equal + sortEmployees(less, increasing)
        }
    }


    /**
     * This function check if s1 is in s2, this method was made by requirement:
     * "No se deben usar funciones nativas o de librería, como por ejemplo el .filter de kotlin, la implementación debe ser propia"
     * But the complexity of this is bigger than string.contains
     */
    fun isSubstring(s1: String, s2: String): Boolean {
        val length1 = s1.length
        val length2 = s2.length

        var i = 0
        var j = 0

        while(i <= length2 - length1) {

            j = 0

            while(j < length1) {
                if(s2[i + j] != s1[j])
                    break
                j++
            }

            if (j == length1)
                return true;

            i++
        }
        return false
    }

}