package com.diegocalero.muyrrhh.views.fragments

import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.fragment.app.Fragment
import com.diegocalero.muyrrhh.R

open class BaseFragment: Fragment() {
    protected fun setOnEditorActionListener(edittextSearch: EditText) {
        edittextSearch.setOnEditorActionListener(TextView.OnEditorActionListener { _, _, _ ->
            val view = activity?.currentFocus
            view?.let { v ->
                val imm =
                    activity?.getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager
                imm?.hideSoftInputFromWindow(v.windowToken, 0)
            }
            true
        })
    }

    protected fun setOnChangeTextListener(edittextSearch: EditText, search: (search: String) -> Unit) {
        edittextSearch.addTextChangedListener(object :
            TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                search(s.toString())
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
        })
    }

    protected fun initSpinner(spinnerSort: Spinner, sortEmployees: (increasing: Boolean) -> Unit) {
        context?.let {
            ArrayAdapter.createFromResource(
                it,
                R.array.sort_strings,
                R.layout.layout_custom_spinner
            ).also { adapter ->
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                spinnerSort.adapter = adapter
            }
        }

        spinnerSort.onItemSelectedListener = (object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {}

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                if(position == 0) {
                    sortEmployees(true)
                } else {
                    sortEmployees(false)
                }
            }
        })
    }
}