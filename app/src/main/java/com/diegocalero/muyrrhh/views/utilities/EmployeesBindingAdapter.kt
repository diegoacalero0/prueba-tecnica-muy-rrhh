package com.diegocalero.muyrrhh.views.utilities

import android.widget.TextView
import androidx.databinding.BindingAdapter
import com.diegocalero.muyrrhh.R

@BindingAdapter("setWage")
fun setWage(textView: TextView, wage: Long?) {
    if(wage == null) {
        textView.setText(textView.resources.getString(R.string.employee_whitout_wage))
    } else {
        textView.setText(textView.resources.getString(R.string.employee_wage, wage))
    }
}