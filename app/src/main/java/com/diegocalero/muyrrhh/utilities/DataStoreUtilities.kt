package com.diegocalero.muyrrhh.utilities

import android.content.Context
import androidx.datastore.preferences.createDataStore
import androidx.datastore.preferences.edit
import androidx.datastore.preferences.preferencesKey
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map


class DataStoreUtilities(val context: Context) {
    private val DATASTORE_NAME = "employeesSetting"
    private val NEWEMPLOYEES_KEY = preferencesKey<String>("newEmployeesSet")

    private val dataStore = context.createDataStore(name = DATASTORE_NAME)

    suspend fun setNewEmployeesMap(newEmployees: HashSet<Int>) {
        dataStore.edit { preferences ->
            val gson = Gson()
            val newEmployeesString = gson.toJson(newEmployees)
            preferences[NEWEMPLOYEES_KEY] = newEmployeesString
        }
    }

    val newEmployeesFlow: Flow<HashSet<Int>?> = dataStore.data
        .map { preferences ->
            val listString = preferences[NEWEMPLOYEES_KEY]?: null

            if(!listString.isNullOrEmpty()) {
                val gson = Gson()
                val arrayListType = object : TypeToken<HashSet<Int>>() {}.type
                gson.fromJson<HashSet<Int>>(listString, arrayListType)
            } else {
                HashSet<Int>()
            }
        }
}